package pl.sda.domowe.zad4.przelaczanie;

public class Przelaczanie {
    public static void main(String[] args) {
        int waga = 80;

        if (waga == 80) {
            System.out.println("Jest ok!");
        } else if (waga == 90) {
            System.out.println("Trochę za ciężki");
        } else if (waga == 100) {
            System.out.println("W sam raz!");
        }

        switch (waga) {
            case 80:
                System.out.println("80");
            case 90:
                System.out.println("90");
                break;
            case 100:
                System.out.println("100");
                break;
            default:
                System.out.println("default");
                break;
        }
    }
}