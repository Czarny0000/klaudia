package pl.sda.domowe.zad4.zad10;

public class Zadanie10 {
    public static void main(String[] args) {
        int x=15,y=53, z=13;

        System.out.println("x="+x+", y="+y+", z="+z);

        //sprawdzanie największej liczby
        if(x>y){ //x jest większe więc sprawdzamy czy x jest większe od z
            if(x>z){ //wetedy x jest największe i wypisujemy ją
                System.out.println("Największa liczba to x="+x);
            }else{ // z jest większe od x ale x jest większe od y więc z>y
                System.out.println("Największa liczba to z="+z);
            }
        }else{ // y jest większe od x ale nie wiemy czy od z
            if(y>z){ // y jest większe od z
                System.out.println("Największa liczba to y="+y);
            }else{ //z jest większe od y
                System.out.println("Największa liczba to z="+z);
            }
        }
        //sprawdzanie najmniejszej liczby:
        if(x<y){
            if(x<z){
                System.out.println("Najmniejsza liczba to x="+x);
            }else{
                System.out.println("Najmniejsza liczba to z="+z);
            }
        }else{
            if(y<z){
                System.out.println("Najmniejsza liczba to y="+y);
            }else{
                System.out.println("Najmniejsza liczba to z="+z);
            }
        }
    }
}
