package pl.sda.domowe.zad4.zad13;

import java.util.Random;
import java.util.Scanner;

public class Zadanie13 {
    public static void main(String[] args) {
        //a
        System.out.println("Podaj liczbę");
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        Random generator = new Random();


        for (int i = 0; i < n; i++) {

            int a = generator.nextInt(100);
            System.out.println(a);
        }
        //b
        System.out.println("Podaj liczbę");
        int m = scanner.nextInt();

        for (int i = 0; i < m; i++) {

            double b = generator.nextDouble();
            int c = generator.nextInt(11);

            System.out.format("%.3f \n", b + c);
        }
        //c
        System.out.println("Podaj liczbę");
        int o = scanner.nextInt();

        for (int i = 0; i < o; i++) {

            boolean d = generator.nextBoolean();

            System.out.println(d);
        }
        //d
        {
            System.out.println("Podaj początek zakresu");
            int poczatekZakresu = scanner.nextInt();
            System.out.println("Podaj koniec zakresu:");
            int koniecZakresu = scanner.nextInt();
            System.out.println("Podaj ilość liczb do wylosowania:");
            int p = scanner.nextInt();

            for (int i = 0; i < p; i++) {
                int e = generator.nextInt((koniecZakresu - poczatekZakresu) + 1) + poczatekZakresu;
                double f = generator.nextDouble();


                System.out.println(e);
            }


        }
    }
}

