package pl.sda.domowe.zad4.petle_3.zad6;

import java.util.Scanner;

public class Zadanie6 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int liczba;
        do {
            System.out.println("Podaj liczbę całkowitą dodatnią:");
            liczba = scanner.nextInt();
        } while (liczba < 0);

        int wypisywana = 1;

        do {
            System.out.println(wypisywana);
            wypisywana*=2;

        } while (wypisywana<liczba);
    }
}
