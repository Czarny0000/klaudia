package pl.sda.domowe.zad4.petle_3.zad7;

import java.util.Scanner;

public class Zadanie7 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile liczb chcesz wpisać:");
        int tabela = scanner.nextInt();
        int[] liczby = new int[tabela];

        for (int i = 0; i < liczby.length; i++) {
            System.out.println("Podaj " + (i + 1) + " liczbę z " + tabela);
            liczby[i] = scanner.nextInt();

        }
        int max = liczby[0];
        for (int l : liczby) {
            if (l > max) {
                max = l;
            }
        }

        int min = liczby[0];
        for (int k : liczby) {

            if (k < max) {
                min = k;
            }

        }
    }
}
