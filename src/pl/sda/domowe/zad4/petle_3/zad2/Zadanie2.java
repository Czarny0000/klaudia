package pl.sda.domowe.zad4.petle_3.zad2;

import java.util.Scanner;

public class Zadanie2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile razy ma być wyświetlony napis:");
        int ilosc = scanner.nextInt();

        System.out.println("Pętla 'for':");
        for (int i = 1; i < (ilosc + 1); i++) {

            System.out.println(i + ". Hello World!");

        }
        System.out.println("Pętla 'while':");

        int iterator = 1;

        while (iterator < (ilosc+1)) {
            System.out.println(iterator + ". Hello World!");

            iterator++;
            }
        }

    }
