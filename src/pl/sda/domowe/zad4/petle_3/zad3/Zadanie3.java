package pl.sda.domowe.zad4.petle_3.zad3;

import java.util.Scanner;

public class Zadanie3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj ile razy ma być wyświetlony napis:");
        int ilosc = scanner.nextInt();

        if (ilosc <= 0) {
            System.out.println("Podana liczba musi być większa od 0!");
        } else {
            for (int i = 1; i < (ilosc + 1); i++) {

                System.out.println(i + ". Hello World!");
            }
        }
    }

}


