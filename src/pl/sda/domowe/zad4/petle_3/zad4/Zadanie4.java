package pl.sda.domowe.zad4.petle_3.zad4;

import java.util.Scanner;

public class Zadanie4 {
    public static void main(String[] args) {
        Scanner scanner =  new Scanner(System.in);

        System.out.println("Podaj początek zakresu:");
        int początekZakresu = scanner.nextInt();

        System.out.println("Podaj koniec zakresu:");
        int koniecZakresu = scanner.nextInt();

        //a
        if (początekZakresu>koniecZakresu){
            System.out.println("Początek zakresu musi być mniejszy niż koniec zakresu!");

        }else {
            System.out.println("Podaj dzielnik:");
            int dzielnik = scanner.nextInt();
            for (int i = początekZakresu; i <= koniecZakresu; i++) {
                if (i%dzielnik==0){
                    System.out.println(i);
                }


            }
        }

    }
}
