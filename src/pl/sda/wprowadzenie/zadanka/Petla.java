package pl.sda.wprowadzenie.zadanka;

public class Petla {
    public static void main(String[] args) {
        //a
        int iterator = 1;
        while (iterator<100) {
            System.out.println(iterator);
            iterator++;
        }
        //e
        int i=30;
        while (i<=300){
            if(i%3==0 && i%5==0){
                System.out.println(i);
            }
            i++;
        }
    }
}

