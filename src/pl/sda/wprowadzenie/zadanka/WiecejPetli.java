package pl.sda.wprowadzenie.zadanka;

public class WiecejPetli {
    public static void main(String[] args) {

        //a wyświetla od 1-100
        for (int i = 1; i <= 100; i++) {
            System.out.println(i);
        }
        //b liczby z zakresu 1000-1020 w tej samej linii
        for (int i = 1000; i <= 1020; i++) {
            System.out.print(", " + i);
        }
        //c liczby podzielne prze 5
        for (int i = -30; i < 1000; i++) {
            if (i % 5 == 0)
                System.out.println(i);
        }
        //d
        for (int i = 1; i < 100; i++) {
            if (i % 3 == 0)
                System.out.println(i);


        }
        //e
        for (int i = 30; i < 300; i++) {
            if (i % 3 == 0 && i % 5 == 0)
                System.out.println(i);
        }
        //f
        for (int i = -30; i < 1000; i++) {
            if (i % 2 != 0)
                System.out.println(i);
        }
        //g
        for (int i = -100; i < 100; i++) {
            System.out.print(i + ";");
        }
        //h
        for (char i = 'a'; i <= 'z'; i++) {
            System.out.println(i);
        }
        //i
        for (char i = 'A'; i <= 'Z'; i++) {
            System.out.println(i);
        }
        //j
        for (char i = 'A'; i <= 'Z'; i = (char) (i + 2)) {
            System.out.println(i);
        }
        //k
        for (char i = 'b'; i <='z' ; i= (char) (i+2)) {
            System.out.println(i);
        }
        //l
        for (int i = 1; i <=100 ; i++) {
            System.out.println(i+". Hello World");
        }
    }
}
