package pl.sda.wprowadzenie.zadanka;

import java.util.Random;
import java.util.Scanner;

public class PodajLiczbe {
    public static void main(String[] args) {
        Scanner scanner= new Scanner(System.in);

        Random generator = new Random();
        int wygenerowana = generator.nextInt(100);

        int liczba;
        do{
            System.out.println("Podaj liczbę:");
            liczba = scanner.nextInt();

            if (liczba >= wygenerowana) {
            System.out.println("Liczba jest za mała");
            }else{
                System.out.println("Liczba jest za duża");
            }
        }while (liczba !=0);

        }
    }

