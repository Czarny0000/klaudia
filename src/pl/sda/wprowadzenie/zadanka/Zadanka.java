package pl.sda.wprowadzenie.zadanka;

public class Zadanka {
    public static void main(String[] args) {
        //a
        if (2 > 3) {
            System.out.println("Spełniony 2>3");
        } else {
            System.out.println("Nie jest spełniony 2>3");
        }
        //b
        if(4<5){
            System.out.println(" :) 4<5");
        }else{
            System.out.println(" :( 4<5");
        }
        //c
        if((2-2)==0){
            System.out.println("(2-2)==0 :) ");
        }else{
            System.out.println("(2-2)==0 :( ");
        }
        //d
        if(true){
            System.out.println("fajnie");
        }else{
            System.out.println("niefajnie");
        }
        //e
        if(9%2==0){
            System.out.println(" e :)");
        }else{
            System.out.println("e :(");
        }

        //f
        if(9%3==0){
            System.out.println("f :)");
        }else {
            System.out.println("f :(");
        }

    }
}
