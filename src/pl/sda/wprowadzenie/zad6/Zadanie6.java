package pl.sda.wprowadzenie.zad6;

public class Zadanie6 {
    public static void main(String[] args) {
        //short -> int
        short zmiennaShort = 3;
        int zmiennaInt = zmiennaShort;

        System.out.println(zmiennaInt);

        // short -> long
        short zmiennaShort2 = 4;
        long zmiennaLong = zmiennaShort2;

        System.out.println(zmiennaShort2);

        // int -> float
        int zmiennaInt2 = 32349;
        float zmiennaFloat = zmiennaInt2;

        System.out.println(zmiennaFloat);

        int c = 49494;
        double d = c;

        System.out.println(d);

        // long -> int
        long e = 123;
        int f = (int) e;

        System.out.println(f);

        // short -> byte
        short g = 456;
        byte h = (byte) g;

        System.out.println(h);

        //char -> int
        char z = 'A';
        int x = z;

        System.out.println(x);
    }
}
