package pl.sda.wprowadzenie.metody.metoda3;

import java.util.Scanner;

public class Kalkulator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj znak działania: + lub - lub * lub /");
        String slowo = scanner.next();

        System.out.println("Podaj pierwszą liczbę");
        int a = scanner.nextInt();
        System.out.println("Podaj drugą liczbę");
        int b = scanner.nextInt();

        switch (slowo) {

            case "+":
                double suma = dodawanie(a, b);
                System.out.println("Wynik dodawania = " + suma);

                break;
            case "-":
                double roznica = odejmowanie(a, b);
                System.out.println("Wynik odejmowania = " + roznica);

                break;
            case "*":
                double iloczyn = mnozenie(a, b);
                System.out.println("Wynik mnożenia = " + iloczyn);

                break;
            case "/":
                double iloraz = dzielenie(a, b);
                System.out.println("Wynik dzielenia= " + iloraz);

                break;
            default:
                System.out.println("Niepoprawne działanie");

        }

    }

    static double dodawanie(double a, double b) {
        return a + b;
    }

    static double odejmowanie(double a, double b) {
        return a - b;
    }

    static double mnozenie(double a, double b) {
        return a * b;
    }

    static double dzielenie(double a, double b) {
        return a / b;
    }


}
