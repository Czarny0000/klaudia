package pl.sda.wprowadzenie.metody.pierwsze_kroki;

import java.util.Scanner;

public class Metoda {
    public static void main(String[] args) {
        int a = 5;
        drukujParzystoscLiczby(a);
        boolean czyParzysta = czyLiczbaJestParzysta(a);
        System.out.println(czyParzysta);
        System.out.println(czyLiczbaJestParzysta2(a));

    }

    public static void drukujParzystoscLiczby(int liczba) {
        if (liczba % 2 == 0) {
            System.out.println("Liczba jest parzysta");
        } else {
            System.out.println("Liczba jest nieparzysta");
        }
    }

    static boolean czyLiczbaJestParzysta(int liczba2) {
        if (liczba2 % 2 == 0) {
            return true;
        }
        return false;
    }

    static boolean czyLiczbaJestParzysta2(int liczba3) {
        return liczba3 % 2 == 0;

    }
}
