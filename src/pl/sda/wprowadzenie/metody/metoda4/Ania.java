package pl.sda.wprowadzenie.metody.metoda4;

public class Ania {
    public static void main(String[] args) {

        String tekst = "babbabababa";

        boolean czy = tekst.contains("aba");
        if (czy){
            System.out.println("1. Tekst zawiera 'aba'");
        }else{
            System.out.println("1. Tekst nie zawiera 'aba'");
        }

        boolean czy2 = tekst.startsWith("aba");
        if (czy2){
            System.out.println("2. Tekst zaczyna się na 'aba'");
        }else{
            System.out.println("2. Tekst nie zaczyna się na 'aba'");
        }

        boolean czy3 = tekst.endsWith("aba");
        if (czy3){
            System.out.println("3. Tekst kończy się na 'aba'");
        }else{
            System.out.println("3. Tekst nie kończy się na 'aba'");
        }
        boolean czy4 = tekst.equals("aba");
        if (czy4){
            System.out.println("4. Tekst równa się słowu 'aba'");
        }else{
            System.out.println("4. Tekst nie równa się słowu 'aba'");
        }


        String tekst2=tekst.toLowerCase();
        boolean czy5 = tekst2.contains("aba");
        if (czy5){
            System.out.println("5. Tekst równa się słowu 'aba'");
        }else{
            System.out.println("5. Tekst nie równa się słowu 'aba'");
        }

        int czy6 = tekst.indexOf("aba");
        if (czy6 !=-1){
            System.out.println("6. Zwraca numer znaku/litery pierwszego wystąpienia 'aba': "+czy6);
        }else{
            System.out.println("6. Tekst nie występuje");
        }


    }


}
