package pl.sda.wprowadzenie.metody.metoda2;

import java.util.Scanner;

public class Metoda2 {
    public static void main(String[] args) {

        System.out.println("Pierwsze zadanie");
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę");
        int liczba = scanner.nextInt();

        boolean czyPodzielne = podzielnePrzez35(liczba);
        System.out.println(czyPodzielne);



        System.out.println("Drugie zadanie");

        System.out.println("Podaj liczbę");
        int liczba2 = scanner.nextInt();
        System.out.println("Podaj liczbę przez którą chcesz podzielić");
        int dzielnik2 = scanner.nextInt();

        boolean czyPodzielnePrzez=podzielnePrzezLiczby(liczba2,dzielnik2);
        System.out.println(czyPodzielnePrzez);

    }

    static boolean podzielnePrzez35(int a) {
        return ((a % 3 == 0) || (a % 5 == 0));

    }

    static boolean podzielnePrzezLiczby(int a, int dzielnik) {
        return (a % dzielnik == 0);

    }
}
